#!/bin/bash

# "mpicxx" is the mpi compiler which underneath will use the default installed compiler, something like "g++".
# "-o app" sets the output file to be named app
mpicxx main.cpp -o app